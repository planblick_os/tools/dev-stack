version: '3'

volumes:
  kong_db_volume: { }
  konga_db_volume: { }
  portainer_db_volume: { }
  mysql_db_volume: { }

networks:
  kong-net:
    external: true

services:
  rabbitmq.planblick.svc:
    build: ../rabbitmq/
    image: rabbitmq
    restart: on-failure
    healthcheck:
      test: rabbitmq-diagnostics -q ping
      interval: 10s
      timeout: 60s
      retries: 12
    environment:
      - RABBITMQ_DEFAULT_USER=${RABBITMQ_DEFAULT_USER}
      - RABBITMQ_DEFAULT_PASS=${RABBITMQ_DEFAULT_PASS}
      - ENVIRONMENT=${ENVIRONMENT}
    stdin_open: true
    tty: true
    networks:
      - kong-net
    ports:
      - "15672:15672"
      - "5672:5672"

  socket-server.planblick.svc:
    build: ../socket-server
    image: socket-server
    restart: always
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
    stdin_open: true
    tty: true
    depends_on:
      kong.planblick.svc:
        condition: service_healthy
      rabbitmq.planblick.svc:
        condition: service_healthy
    networks:
      - kong-net
    ports:
      - "5000:5000"
    volumes:
      - ../socket-server/src/app:/src/app

  kong-migrations:
    image: "kong:2.3.3-alpine"
    command: kong migrations bootstrap
    depends_on:
      postgres.planblick.svc:
        condition: service_healthy
    environment:
      - KONG_CASSANDRA_CONTACT_POINTS=postgres.planblick.svc
      - KONG_DATABASE=postgres
      - KONG_PG_DATABASE=${KONG_DATABASE}
      - KONG_PG_HOST=postgres.planblick.svc
      - KONG_PG_PASSWORD=${KONG_PG_PASSWORD}
      - KONG_PG_USER=${KONG_PG_USER}
    links:
      - postgres.planblick.svc
    networks:
      - kong-net
    restart: on-failure

  postgres.planblick.svc:
    image: postgres:9.5
    environment:
      - POSTGRES_DB=${KONG_DATABASE}
      - POSTGRES_USER=${KONG_PG_USER}
      - POSTGRES_PASSWORD=${KONG_PG_PASSWORD}
    healthcheck:
      test: [ "CMD", "pg_isready", "-U", "kong" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: on-failure
    stdin_open: true
    tty: true
    networks:
      - kong-net
    ports:
      - "5432:5432/tcp"
    volumes:
      - kong_db_volume:/var/lib/postgresql/data

  konga:
    image: pantsel/konga:latest
    restart: always
    networks:
      - kong-net
    environment:
      - TOKEN_SECRET=km1GUr4RkcQD7DewhJPNXrCuZwcKmqjb
      - NODE_ENV=${ENVIRONMENT}
      - KONGA_SEED_USER_DATA_SOURCE_FILE=/tmp/kong_config/default_users_seed.data
      - KONGA_SEED_KONG_NODE_DATA_SOURCE_FILE=/tmp/kong_config/default_connections.data
    links:
      - postgres.planblick.svc
    ports:
      - "1337:1337"
    volumes:
      - ./kong_config:/tmp/kong_config

  kong.planblick.svc:
    image: "kong:2.3.3-alpine"
    depends_on:
      postgres.planblick.svc:
        condition: service_healthy
    environment:
      - KONG_ADMIN_ACCESS_LOG=/dev/null
      - KONG_ADMIN_ERROR_LOG=/dev/stderr
      - KONG_ADMIN_LISTEN=0.0.0.0:8001
      - KONG_CASSANDRA_CONTACT_POINTS=postgres.planblick.svc
      - KONG_DATABASE=postgres
      - KONG_PG_DATABASE=${KONG_DATABASE}
      - KONG_PG_HOST=postgres.planblick.svc
      - KONG_PG_PASSWORD=${KONG_PG_PASSWORD}
      - KONG_PG_USER=${KONG_PG_USER}
      - KONG_PROXY_ACCESS_LOG=/dev/null
      - KONG_PROXY_ERROR_LOG=/dev/stderr
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://localhost:8001/" ]
      interval: 10s
      timeout: 60s
      retries: 12
    networks:
      - kong-net
    ports:
      - "8000:8000/tcp"
      - "8001:8001/tcp"
      - "8443:8443/tcp"
      - "8444:8444/tcp"
    restart: on-failure

  mysql.planblick.svc:
    image: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    healthcheck:
      test: [ "CMD", 'mysqladmin', 'ping', '-h', 'localhost', '-u', 'root', '-p$$MYSQL_ROOT_PASSWORD' ]
      interval: 10s
      timeout: 10s
      retries: 12
    environment:
      - MYSQL_DATABASE=${MYSQL_DATABASE}
      - MYSQL_USER=${MYSQL_USER}
      - MYSQL_PASSWORD=${MYSQL_PASSWORD}
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
    ports:
      - ${MYSQL_PORT}:3306
    networks:
      - kong-net
    volumes:
      - mysql_db_volume:/var/lib/mysql

  smtpsink.planblick.svc:
    build: ../smtpsink
    image: smtpsink
    stdin_open: true
    restart: always
    tty: true
    ports:
      - "10025:10025"
    networks:
      - kong-net
    volumes:
      - ../smtpsink/src/app:/src/app

  account-manager.planblick.svc:
    build: ../account-manager
    image: account-manager
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=root
      - DB_PASSWORD=password
      - DB_NAME=account-manager
      - DB_DEBUG=no
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - SMTP_HOST=${SMTP_HOST}
      - SMTP_PORT=${SMTP_PORT}
      - SMTP_AUTH_NEEDED=${SMTP_AUTH_NEEDED}
      - SMTP_SSL=${SMTP_SSL}
      - SMTP_USERNAME=${SMTP_USERNAME}
      - SMTP_PASSWORD=${SMTP_PASSWORD}
      - MAIL_CERTIFICATES_KEY_PATH=${MAIL_CERTIFICATES_KEY_PATH}
      - MAIL_CERTIFICATES_CRT_PATH=${MAIL_CERTIFICATES_CRT_PATH}
      - CONTACT_MAIL_RECIPIENT=flanna@esports-online.eu
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
      - SYSTEM_FRONTEND_BASEDOMAIN=${SYSTEM_FRONTEND_BASEDOMAIN}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:80/healthz" ]
      interval: 10s
      timeout: 60s
      retries: 12
    stdin_open: true
    tty: true
    restart: always
    depends_on:
      kong.planblick.svc:
        condition: service_healthy
      rabbitmq.planblick.svc:
        condition: service_healthy
      mysql.planblick.svc:
        condition: service_healthy
    networks:
      - kong-net
    ports:
      - "8020:80"
    volumes:
      - ../account-manager/src/app:/src/app
      - ../account-manager/src/certs:/src/certs

  stack-bootstrapper.planblick.svc:
    build: ../stack-bootstrapper
    image: stack-bootstrapper
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - SYSTEM_USER=${SYSTEM_USER}
      - SYSTEM_PASSWORD=${SYSTEM_PASSWORD}
    stdin_open: true
    tty: true
    ports:
      - "8021:8000"
    volumes:
      - ../stack-bootstrapper/src/app:/src/app
    networks:
      - kong-net

  frontend-templates:
    build: ../frontend-templates
    container_name: frontend-templates
    environment:
      URL: localhost
    expose:
      - "80"
      - "443"
    ports:
      - "7000:80"
    volumes:
      - ../frontend-templates/src/app:/src/app
    domainname: localhost
    restart: always
    networks:
      - kong-net

  eventstore.planblick.svc:
    build: ../eventstore
    image: eventstore
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - INTERNAL_ACCOUNTMANAGER_URL=${INTERNAL_ACCOUNTMANAGER_URL}
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=eventstore
      - DB_DEBUG=no
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
      - EVENT_CRYPTOGRAPHY_KEY=${EVENT_CRYPTOGRAPHY_KEY}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    stdin_open: true
    tty: true
    networks:
      - kong-net
    ports:
      - "8003:8000"
    volumes:
      - ../eventstore/src/app:/src/app
      - ../eventstore/src/certs:/src/certs

  easy2schedule.planblick.svc:
    build: ../easy2schedule
    image: easy2schedule
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - INTERNAL_ACCOUNTMANAGER_URL=${INTERNAL_ACCOUNTMANAGER_URL}
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=easy2schedule
      - DB_DEBUG=no
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - SMTP_HOST=${SMTP_HOST}
      - SMTP_PORT=${SMTP_PORT}
      - SMTP_AUTH_NEEDED=${SMTP_AUTH_NEEDED}
      - SMTP_SSL=${SMTP_SSL}
      - SMTP_USERNAME=${SMTP_USERNAME}
      - SMTP_PASSWORD=${SMTP_PASSWORD}
      - MAIL_CERTIFICATES_KEY_PATH=${MAIL_CERTIFICATES_KEY_PATH}
      - MAIL_CERTIFICATES_CRT_PATH=${MAIL_CERTIFICATES_CRT_PATH}
      - SLACK_NOTIFICATION_HOOK=${SLACK_NOTIFICATION_HOOK}
      - ENVIRONMENT=${ENVIRONMENT}
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
      - ACCOUNTMANAGER_STORE_BASE_URL=${ACCOUNTMANAGER_STORE_BASE_URL}
    stdin_open: true
    tty: true
    restart: always
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 60s
      retries: 12
      start_period: 20s
    depends_on:
      kong.planblick.svc:
        condition: service_healthy
      rabbitmq.planblick.svc:
        condition: service_healthy
      mysql.planblick.svc:
        condition: service_healthy
    networks:
      - kong-net
    ports:
      - "8002:8000"
    volumes:
      - ../easy2schedule/src/app:/src/app
      - ../easy2schedule/src/certs:/src/certs

  easy2track.planblick.svc:
    build: ../easy2track
    image: easy2track
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=easy2track
      - DB_DEBUG="no"
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - SMTP_HOST=${SMTP_HOST}
      - SMTP_PORT=${SMTP_PORT}
      - SMTP_AUTH_NEEDED=${SMTP_AUTH_NEEDED}
      - SMTP_SSL=${SMTP_SSL}
      - SMTP_USERNAME=${SMTP_USERNAME}
      - SMTP_PASSWORD=${SMTP_PASSWORD}
      - MAIL_CERTIFICATES_KEY_PATH=${MAIL_CERTIFICATES_KEY_PATH}
      - MAIL_CERTIFICATES_CRT_PATH=${MAIL_CERTIFICATES_CRT_PATH}
      - EASY2TRACK_EXPORT_REQUEST_MAIL_RECIPIENT=${EASY2TRACK_EXPORT_REQUEST_MAIL_RECIPIENT}
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "18000:8000"
    volumes:
      - ../easy2track/src/app:/src/app
      - ../easy2track/src/certs:/src/certs

  certificate-store.planblick.svc:
    build: ../certificate-store
    image: certificate-store
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "19000:8000"
    volumes:
      - ../certificate-store/src/app:/src/app
      - ../certificate-store/src/certs:/src/certs

  cron.planblick.svc:
    environment:
      - SYSTEM_USER=${SYSTEM_USER}
      - SYSTEM_PASSWORD=${SYSTEM_PASSWORD}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
    build: ../cron
    restart: always
    depends_on:
      kong.planblick.svc:
        condition: service_healthy
    networks:
      - kong-net
    stdin_open: true
    tty: true
    volumes:
      - ../cron/tasks:/etc/periodic:ro
      - ../cron/crontab:/etc/crontabs/root:ro

  contact-manager.planblick.svc:
    build: ../contact-manager
    image: contact-manager
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=easycontact
      - DB_DEBUG="no"
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
      - CONTACT_CRYPTOGRAPHY_KEY=${CONTACT_CRYPTOGRAPHY_KEY}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "19001:8000"
    volumes:
      - ../contact-manager/src/app:/src/app

  froala-cdn:
    build: ../froala-cdn
    container_name: froala
    environment:
      URL: localhost
    ports:
      - "3080:80"
    domainname: localhost
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true

  froala-backend.planblick.svc:
    build: ../froala-backend
    image: froala-backend
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - FROALA_BACKEND_BASE_URL=${FROALA_BACKEND_BASE_URL}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "19002:8000"
    volumes:
      - ../froala-backend/src/app:/src/app

  queue-manager.planblick.svc:
      build: ../queue-manager
      image: queue-manager
      environment:
        - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
        - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
        - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
        - DB_HOST=mysql.planblick.svc
        - DB_PORT=${MYSQL_PORT}
        - DB_DRIVER=mysql+pymysql
        - DB_USER=${MYSQL_USER}
        - DB_PASSWORD=${MYSQL_PASSWORD}
        - DB_NAME=queuemanager
        - DB_DEBUG="no"
        - MQ_HOST=rabbitmq.planblick.svc
        - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
        - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
        - MQ_PORT=5672
        - MQ_VHOST=/
        - INTERNAL_ACCOUNTMANAGER_URL=${INTERNAL_ACCOUNTMANAGER_URL}
        - SMTP_HOST=${SMTP_HOST}
        - SMTP_PORT=${SMTP_PORT}
        - SMTP_AUTH_NEEDED=${SMTP_AUTH_NEEDED}
        - SMTP_SSL=${SMTP_SSL}
        - SMTP_USERNAME=${SMTP_USERNAME}
        - SMTP_PASSWORD=${SMTP_PASSWORD}
        - MAIL_CERTIFICATES_KEY_PATH=${MAIL_CERTIFICATES_KEY_PATH}
        - MAIL_CERTIFICATES_CRT_PATH=${MAIL_CERTIFICATES_CRT_PATH}
        - SLACK_NOTIFICATION_HOOK=${SLACK_NOTIFICATION_HOOK}
        - ENVIRONMENT=${ENVIRONMENT}
        - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
        - ACCOUNTMANAGER_STORE_BASE_URL=${ACCOUNTMANAGER_STORE_BASE_URL}
      healthcheck:
        test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
        interval: 10s
        timeout: 10s
        retries: 12
      restart: always
      networks:
        - kong-net
      stdin_open: true
      tty: true
      ports:
        - "19003:8000"
      volumes:
        - ../queue-manager/src/app:/src/app

  qrcodelinker.planblick.svc:
    build: ../qrcodelinker
    image: qrcodelinker
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=qrcodelinker
      - DB_DEBUG="no"
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
      - APIGATEWAY_EXTERNAL_BASEURL=${APIGATEWAY_EXTERNAL_BASEURL}
      - INTERNAL_ACCOUNTMANAGER_URL=${INTERNAL_ACCOUNTMANAGER_URL}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "19004:8000"
    volumes:
      - ../qrcodelinker/src/app:/src/app

  commentservice.planblick.svc:
    build: ../comment-service
    image: commentservice
    environment:
      - APIGATEWAY_ADMIN_INTERNAL_BASEURL=${APIGATEWAY_ADMIN_INTERNAL_BASEURL}
      - APIGATEWAY_INTERNAL_BASEURL=${APIGATEWAY_INTERNAL_BASEURL}
      - INTERNAL_API_BASE_URL=https://kong.planblick.svc:8001
      - DB_HOST=mysql.planblick.svc
      - DB_PORT=${MYSQL_PORT}
      - DB_DRIVER=mysql+pymysql
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
      - DB_NAME=commentservice
      - DB_DEBUG="no"
      - MQ_HOST=rabbitmq.planblick.svc
      - MQ_USERNAME=${RABBITMQ_DEFAULT_USER}
      - MQ_PASSWORD=${RABBITMQ_DEFAULT_PASS}
      - MQ_PORT=5672
      - MQ_VHOST=/
      - CERTIFICATE_STORE_BASE_URL=${CERTIFICATE_STORE_BASE_URL}
    healthcheck:
      test: [ "CMD-SHELL", "wget -O /dev/null -o /dev/null http://127.0.0.1:8000/healthz" ]
      interval: 10s
      timeout: 10s
      retries: 12
    restart: always
    networks:
      - kong-net
    stdin_open: true
    tty: true
    ports:
      - "19005:8000"
    volumes:
      - ../comment-service/src/app:/src/app